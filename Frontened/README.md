# Book Directory in Python

## Book directory App 
### Book Directory is a database app built with Python, Tkinter, and SQLite.

## Why Book Directory
### Book directory acts as a library management system and maintain the records of all the books along with their ISBN number.

# Objectives
## This application contains following objectives:
###To create the database for the books using Mysql.To connect the database to backend.py file and write the functions for CRUD operations.To manage the database using backend.py.To connect the GUI with backend and show the data. 
# The following tasks should be performed:
* View all entries
* Search entries
* Delete entries
* Add entries
* Update selected 
* Close

* Technologies Utilized
* Tkinter
* SQLite
* Key Concepts Applied
* Data Types
* Operators
* Looping
* Functions
* Modules
* Files & I/O

## Learnings
### The main steps for working with a SQLite database are connecting to a database, creating a cursor object, writing an SQL query, commiting changes, and closing the database connection.GUIs can be built with Tkinter windows and widgets. Tkinter arranges label, entry, and button widgets in a window using a grid layout. The button widgets can be linked to functions and the data in entry widgets can be extracted for use elsewhere.Various Python files can interact with each other as modules. This allows for the principle of abstraction, where code can be used without knowing precisely how it was implemented. For this project, the frontend and backend were developed independently and later connected.Workflow of the application.
## The procedure of developing the application follows these steps:
* Create a database and tables to store the data of the books.
* Install and import the required libraries to connect the database to the backend file.
* Create functions to manage the tables using CRUD operations.
* Import the backend file in frontend.py file.
* Use tkinter components to create the GUI and integrate it with the backend functions.

### SQLite is a software library that provides a relational database management system. The lite in SQLite means light weight in terms of setup, database administration, and required resource.SQLite has the following noticeable features: self-contained, serverless, zero-configuration, transactional.

## Serverless
### Normally, an RDBMS such as MySQL, PostgreSQL, etc., requires a separate server process to operate. The applications that want to access the database server use TCP/IP protocol to send and receive requests. This is called client/server architecture.


## RDBMS Client Server Architecture
# SQLite does NOT work this way.

### SQLite does NOT require a server to run.SQLite database is integrated with the application that accesses the database. The applications interact with the SQLite database read and write directly from the database files stored on disk.

![Step1](book/s3.png)
--------------------------------------------------------------------------------------------------
![Step-2](book/s1.png)
--------------------------------------------------------------------------------------------------
![Step-3](book/s2.png)
--------------------------------------------------------------------------------------------------
![Step-4](book/s7.png)
--------------------------------------------------------------------------------------------------
![Step-5](book/s5.png)
--------------------------------------------------------------------------------------------------
![Step-6](book/s4.png)
--------------------------------------------------------------------------------------------------
![Step-7](book/s6.png)
--------------------------------------------------------------------------------------------------