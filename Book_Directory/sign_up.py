from tkinter import *


root=Tk()

root.geometry("1350x650+0+0")

root.title("Book Directory")

f1 = Frame(root, width=1400, height=30, relief=SUNKEN)
f1.pack(side=TOP)

f2 = Frame(root, width=1400, height=650, bg="powderblue", relief=SUNKEN)
f2.pack(side=LEFT)

f3 = Frame(root)
f3.place(x=500, y=300)

Heading = Label(f1, text="Book Directory App  Create Account ?", font=('arial', 40, 'bold'))
Heading.grid(row=0, column=0)


# define labels
l1 = Label(f2, text="User-Name", font=('arial', 15, 'bold'), bg="powderblue")
l1.place(x=300, y=30)

l2 = Label(f2, text="Password", font=('arial', 15, 'bold'), bg="powderblue")
l2.place(x=300, y=80)

l3 = Label(f2, text="Mobile-No", font=('arial', 15, 'bold'), bg="powderblue")
l3.place(x=300, y=130)

# Entry section
name = StringVar()
e1 = Entry(f2, font=('arial', 15, 'bold'), textvariable=name, insertwidth=5, justify=LEFT, bd=5, width=35)
e1.place(x=500, y=30)

department = StringVar()
e2 = Entry(f2, font=('arial', 15, 'bold'), textvariable=department, insertwidth=5, justify=LEFT, bd=5, width=35)
e2.place(x=500, y=80)

roll_no = StringVar()
e3 = Entry(f2, font=('arial', 15, 'bold'), textvariable=roll_no, insertwidth=5, justify=LEFT, bd=5, width=35)
e3.place(x=500, y=130)

# define button

b1 = Button(f2, text="Create Account", font=('arial', 10, 'bold'), bd=5, width=15, height=2, bg='#3D6DEE')
b1.place(x=600, y=200)


root.mainloop()