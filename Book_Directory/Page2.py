from tkinter import *


root=Tk()

root.geometry("1350x650+0+0")

root.title("Book Directory")

f1 = Frame(root, width=1400, height=70, relief=SUNKEN)
f1.pack(side=TOP)

f2 = Frame(root, width=1400, height=650, bg="powderblue", relief=SUNKEN)
f2.pack(side=LEFT)

f3 = Frame(root)
f3.place(x=200, y=200)

Heading = Label(f1, text="Book Search", font=('arial', 40, 'bold'))
Heading.place(x=500, y=0)

# define labels
l1 = Label(f2, text="Book Name", font=('arial', 15, 'bold'), bg='powderblue')
l1.place(x=10, y=20)

l5 = Label(f2, text="Language", font=('arial', 15, 'bold'), bg='powderblue')
l5.place(x=10, y=70)

# Entry section
book = StringVar()
e1 = Entry(f2, font=('arial', 15), textvariable=book, insertwidth=5, justify=LEFT, bd=5, width=48)
e1.place(x=200, y=20)

lang = StringVar()
e6 = Entry(f2, font=('arial', 15), textvariable=lang, insertwidth=5, justify=LEFT, bd=5, width=48)
e6.place(x=200, y=70)

# define button

b1 = Button(f2, text="Search", font=('arial', 10, 'bold'), bd=5, width=15, height=2, bg='#3D6DEE')
b1.place(x=800, y=20)

b2 = Button(f2, text="Issue Book", font=('arial', 10, 'bold'), bd=5, width=15, height=2, bg='#3D6DEE')
b2.place(x=1100, y=20)

s = Scrollbar(f3)
s.pack(side=RIGHT, fill=Y)
l = Listbox(f3, width=86, height=27, yscrollcommand=s.set)
l.pack(side=LEFT, fill=BOTH)

b3 = Button(f2, text="Back", font=('arial', 10, 'bold'), width=15, bd=5, height=2, bg='#3D6DEE')
b3.place(x=1100, y=405)

b4 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=15, bd=5, height=2, bg='#3D6DEE')
b4.place(x=1100, y=505)


root.mainloop()