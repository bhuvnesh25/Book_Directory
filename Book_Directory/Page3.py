from tkinter import *


root=Tk()

root.geometry("1350x650+0+0")


root.title("Book Directory")

f1 = Frame(root, width=1400, height=50, relief=SUNKEN)
f1.pack(side=TOP)

f2 = Frame(root, width=1400, height=650, bg="powderblue", relief=SUNKEN)
f2.pack(side=LEFT)

f3 = Frame(root)
f3.place(x=850, y=100)

Heading = Label(f1, text="Issue Book", font=('arial', 40, 'bold'))
Heading.grid(row=0, column=0)

# define labels
l1 = Label(f2, text="Name of Student", font=('arial', 15, 'bold'), bg="powderblue")
l1.place(x=20, y=30)

l2 = Label(f2, text="Department", font=('arial', 15, 'bold'), bg="powderblue")
l2.place(x=20, y=80)

l3 = Label(f2, text="Roll-No", font=('arial', 15, 'bold'), bg="powderblue")
l3.place(x=20, y=130)

l4 = Label(f2, text="Mobile-No", font=('arial', 15, 'bold'), bg="powderblue")
l4.place(x=20, y=180)

l5 = Label(f2, text="Name of Books", font=('arial', 15, 'bold'), bg="powderblue")
l5.place(x=20, y=230)

l6 = Label(f2, text="Issued-Date", font=('arial', 15, 'bold'), bg="powderblue")
l6.place(x=20, y=280)

l7 = Label(f2, text="Emai-ID", font=('arial', 15, 'bold'), bg="powderblue")
l7.place(x=20, y=330)
# Entry section
name = StringVar()
e1 = Entry(f2, font=('arial', 15, 'bold'), textvariable=name, insertwidth=5, justify=LEFT, bd=5, width=40)
e1.place(x=250, y=30)

department = StringVar()
e2 = Entry(f2, font=('arial', 15, 'bold'), textvariable=department, insertwidth=5, justify=LEFT, bd=5, width=40)
e2.place(x=250, y=80)

roll_no = StringVar()
e3 = Entry(f2, font=('arial', 15, 'bold'), textvariable=roll_no, insertwidth=5, justify=LEFT, bd=5, width=40)
e3.place(x=250, y=130)

mobile = StringVar()
e4 = Entry(f2, font=('arial', 15, 'bold'), textvariable=mobile, insertwidth=5, justify=LEFT, bd=5, width=40)
e4.place(x=250, y=180)

books = StringVar()
e5 = Entry(f2, font=('arial', 15, 'bold'), textvariable=books, insertwidth=5, justify=LEFT, bd=5, width=40)
e5.place(x=250, y=230)

date=StringVar()
e6 = Entry(f2, font=('arial', 15, 'bold'), textvariable=date, insertwidth=5, justify=LEFT, bd=5, width=40)
e6.place(x=250, y=280)

email = StringVar()
e7 = Entry(f2, font=('arial', 15, 'bold'), textvariable=email, insertwidth=5, justify=LEFT, bd=5, width=40)
e7.place(x=250, y=330)
# define button

b1 = Button(f2, text="Issued", font=('arial', 10, 'bold'), bd=5, width=15, height=2, bg='#3D6DEE')
b1.place(x=250, y=450)

s = Scrollbar(f3)
s.pack(side=RIGHT, fill=Y)
l = Listbox(f3, width=75, height=28, yscrollcommand=s.set)
l.pack(side=LEFT, fill=BOTH)

b2 = Button(f2, text="Back", font=('arial', 10, 'bold'), width=15, bd=5, height=2, bg='#3D6DEE',)
b2.place(x=850, y=500)

b3 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=15, bd=5, height=2, bg='#3D6DEE')
b3.place(x=1110, y=500)

root.mainloop()