
import sqlite3


def database():
    """Set up a connection with the database."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS Library (id integer PRIMARY KEY, title text, author text, year integer, isbn integer,language text,placed text)")
    con.commit()
    con.close()


def add(title, author, year, isbn,lang,placed):
    """Insert entry into database."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("INSERT INTO Library VALUES (NULL, ?, ?, ?, ?, ?, ?)", (title, author, year, isbn,lang,placed))
    con.commit()
    con.close()


def view_all():
    """View all database entries."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("SELECT * FROM Library")
    rows = cur.fetchall()
    con.close()
    return rows


def update(id, title, author, year, isbn,lang,placed):
    """Update a database entry."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("UPDATE Libraray  SET title = ?,author = ?,year = ?, isbn = ? ,lang = ? ,placed = ?, WHERE id = ?",(title, author, year, isbn,lang,placed, id))
    con.commit()
    con.close()


def delete(id):
    """Delete a database entry."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("DELETE FROM Library  WHERE id = ?", (id))
    con.commit()
    con.close()


def search(title="", lang=""):
    """Search for a database entry."""
    con = sqlite3.connect("Library.db")
    cur = con.cursor()
    cur.execute("SELECT * FROM Library WHERE title = ?  OR  language = ? ", (title, lang))
    rows = cur.fetchall()
    con.close()
    return rows




database()
