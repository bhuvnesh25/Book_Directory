from tkinter import *
import backend

def add_command():
    """Insert entry via button."""
    backend.add(title.get(),author.get(),year.get(),isbn.get(),lang.get(),placed.get())
    l.delete(0, END)
    l.insert(END,(title.get(),author.get(),year.get(),isbn.get(),lang.get(),placed.get()))

def view_command():
    """View entries via button."""
    l.delete(0, END)
    for row in backend. view_all():
        l.insert(END, row)

def update_command():
   #"""Update entry via button.""
    backend.update(selected_tuple[0],title.get(),author.get(),year.get(),isbn.get(),lang.get(),placed.get())

def delete_command():
    """Delete entry via button."""
    backend.delete(selected_tuple[0])

def search_command():
    """Search entry via button."""
    l.delete(0, END)
    for row in backend.search(title.get(),lang.get()):
        l.insert(END, row)

def get_selected_row(event):
    """Pre-fill fields for selected entry."""
    global selected_tuple
    index = l.curselection()[0]
    selected_tuple = l.get(index)

    e1.delete(0, END)
    e1.insert(END, selected_tuple[1])

    e2.delete(0, END)
    e2.insert(END, selected_tuple[2])

    e3.delete(0, END)
    e3.insert(END, selected_tuple[3])

    e4.delete(0, END)
    e4.insert(END, selected_tuple[4])

    e5.delete(0, END)
    e5.insert(END, selected_tuple[5])

    e6.delete(0, END)
    e6.insert(END, selected_tuple[6])

root=Tk()
# root.wm_iconphoto(default='logo.png')
f1 = Frame(root, width=1370, height=30, bg="lightgreen", relief=SUNKEN)
f1.pack(side=TOP)

f2 = Frame(root, width=1370, height=650, bg="lightgreen", relief=SUNKEN)
f2.pack(side=LEFT)

f3 = Frame(root)
f3.place(x=800, y=100)

Heading = Label(f1, text="Book Directory or Library", font=('arial', 40, 'bold'))
Heading.grid(row=0, column=0)

# define labels
l1 = Label(f2, text="Title", font=('arial', 20, 'bold'), bg='lightgreen')
l1.place(x=10, y=30)

l2 = Label(f2, text="Author", font=('arial', 20, 'bold'), bg='lightgreen')
l2.place(x=10, y=80)

l3 = Label(f2, text="Year", font=('arial', 20, 'bold'), bg='lightgreen')
l3.place(x=10, y=130)

l4 = Label(f2, text="ISBN", font=('arial', 20, 'bold'), bg='lightgreen')
l4.place(x=10, y=180)

l5 = Label(f2, text="Language", font=('arial', 20, 'bold'), bg='lightgreen')
l5.place(x=10, y=230)

l6 = Label(f2, text="Placed", font=('arial', 20, 'bold'), bg='lightgreen')
l6.place(x=10, y=280)
# Entry section
title = StringVar()
e1 = Entry(f2, font=('arial', 20), textvariable=title, insertwidth=5, justify=LEFT, bd=5, width=40)
e1.place(x=150, y=30)

author = StringVar()
e2 = Entry(f2, font=('arial', 20), textvariable=author, insertwidth=5, justify=LEFT, bd=5, width=40)
e2.place(x=150, y=80)

year = StringVar()
e3 = Entry(f2, font=('arial', 20), textvariable=year, insertwidth=5, justify=LEFT, bd=5, width=40)
e3.place(x=150, y=130)

isbn = StringVar()
e4 = Entry(f2, font=('arial', 20), textvariable=isbn, insertwidth=5, justify=LEFT, bd=5, width=40)
e4.place(x=150, y=180)

lang = StringVar()
e5 = Entry(f2, font=('arial', 20), textvariable=lang, insertwidth=5, justify=LEFT, bd=5, width=40)
e5.place(x=150, y=230)

placed = StringVar()
e6 = Entry(f2, font=('arial', 20), textvariable=placed, insertwidth=5, justify=LEFT, bd=5, width=40)
e6.place(x=150, y=280)

# define button

b1 = Button(f2, text="View All", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE',command=view_command)
b1.place(x=150, y=380)

b2 = Button(f2, text="Search Book", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE',command=search_command)
b2.place(x=150, y=480)

b3 = Button(f2, text="Add Entry", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=add_command)
b3.place(x=450, y=380)

b3 = Button(f2, text="Update", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=update_command)
b3.place(x=450, y=480)

s = Scrollbar(f3)
s.pack(side=RIGHT, fill=Y)
l = Listbox(f3, width=80, height=26, yscrollcommand=s.set)
l.pack(side=LEFT, fill=BOTH)
l.bind('<<ListboxSelect>>', get_selected_row)

b4 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=exit)
b4.place(x=1080, y=480)






root.mainloop()