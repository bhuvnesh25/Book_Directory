from tkinter import *
import time

def pageone():
    t = Toplevel(root)
    def pagetwo():
        k=Toplevel(t)
        def pagethird():
            j=Toplevel(k)
            f1 = Frame(j, width=1370, height=30, relief=SUNKEN)
            f1.pack(side=TOP)

            f2 = Frame(j, width=1370, height=650, bg="powderblue", relief=SUNKEN)
            f2.pack(side=LEFT)

            f3 = Frame(j)
            f3.place(x=850, y=100)

            Heading = Label(f1, text="Issue Book", font=('arial', 40, 'bold'))
            Heading.grid(row=0, column=0)

            # define labels
            l1 = Label(f2, text="Name of Student", font=('arial', 20, 'bold'), bg="powderblue")
            l1.place(x=0, y=30)

            l2 = Label(f2, text="Department", font=('arial', 20, 'bold'), bg="powderblue")
            l2.place(x=0, y=80)

            l3 = Label(f2, text="Roll-No", font=('arial', 20, 'bold'), bg="powderblue")
            l3.place(x=0, y=130)

            l4 = Label(f2, text="Mobile-No", font=('arial', 20, 'bold'), bg="powderblue")
            l4.place(x=0, y=180)

            l5 = Label(f2, text="Name of Books", font=('arial', 20, 'bold'), bg="powderblue")
            l5.place(x=0, y=230)

            l6 = Label(f2, text="Issued-Date", font=('arial', 20, 'bold'), bg="powderblue")
            l6.place(x=0, y=280)

            l7 = Label(f2, text="Emai-ID", font=('arial', 20, 'bold'), bg="powderblue")
            l7.place(x=0, y=330)
            # Entry section
            name = StringVar()
            e1 = Entry(f2, font=('arial', 20, 'bold'), textvariable=name, insertwidth=5, justify=LEFT, bd=5, width=35)
            e1.place(x=250, y=30)

            department = StringVar()
            e2 = Entry(f2, font=('arial', 20, 'bold'), textvariable=department, insertwidth=5, justify=LEFT, bd=5,
                       width=35)
            e2.place(x=250, y=80)

            roll_no = StringVar()
            e3 = Entry(f2, font=('arial', 20, 'bold'), textvariable=roll_no, insertwidth=5, justify=LEFT, bd=5,
                       width=35)
            e3.place(x=250, y=130)

            mobile = StringVar()
            e4 = Entry(f2, font=('arial', 20, 'bold'), textvariable=mobile, insertwidth=5, justify=LEFT, bd=5, width=35)
            e4.place(x=250, y=180)

            books = StringVar()
            e5 = Entry(f2, font=('arial', 20, 'bold'), textvariable=books, insertwidth=5, justify=LEFT, bd=5, width=35)
            e5.place(x=250, y=230)

            localtime = time.asctime()
            e6 = Entry(f2, font=('arial', 20, 'bold'), textvariable=localtime, insertwidth=5, justify=LEFT, bd=5,
                       width=35)
            e6.place(x=250, y=280)

            email = StringVar()
            e7 = Entry(f2, font=('arial', 20, 'bold'), textvariable=email, insertwidth=5, justify=LEFT, bd=5, width=35)
            e7.place(x=250, y=330)
            # define button

            b1 = Button(f2, text="Issued", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE')
            b1.place(x=250, y=450)

            s = Scrollbar(f3)
            s.pack(side=RIGHT, fill=Y)
            l = Listbox(f3, width=75, height=28, yscrollcommand=s.set)
            l.pack(side=LEFT, fill=BOTH)

            b2 = Button(f2, text="Back", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=pagetwo )
            b2.place(x=850, y=500)

            b3 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=exit)
            b3.place(x=1110, y=500)
            t.withdraw()
            mainloop()

        f1 = Frame(k, width=1370, height=70, relief=SUNKEN)
        f1.pack(side=TOP)

        f2 = Frame(k, width=1370, height=650, bg="powderblue", relief=SUNKEN)
        f2.pack(side=LEFT)

        f3 = Frame(k)
        f3.place(x=200, y=200)

        Heading = Label(f1, text="Book Search", font=('arial', 40, 'bold'))
        Heading.place(x=500, y=0)

        # define labels
        l1 = Label(f2, text="Book Name", font=('arial', 20, 'bold'), bg='powderblue')
        l1.place(x=10, y=20)

        l5 = Label(f2, text="Language", font=('arial', 20, 'bold'), bg='powderblue')
        l5.place(x=10, y=70)

        # Entry section
        book = StringVar()
        e1 = Entry(f2, font=('arial', 20, 'bold'), textvariable=book, insertwidth=5, justify=LEFT, bd=5, width=35)
        e1.place(x=200, y=20)

        lang = StringVar()
        e6 = Entry(f2, font=('arial', 20, 'bold'), textvariable=lang, insertwidth=5, justify=LEFT, bd=5, width=35)
        e6.place(x=200, y=70)

        # define button

        b1 = Button(f2, text="Search", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE')
        b1.place(x=800, y=20)

        b2 = Button(f2, text="Issue Book", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE',command=pagethird)
        b2.place(x=1100, y=20)

        s = Scrollbar(f3)
        s.pack(side=RIGHT, fill=Y)
        l = Listbox(f3, width=86, height=25, yscrollcommand=s.set)
        l.pack(side=LEFT, fill=BOTH)

        b3 = Button(f2, text="Back", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=pageone)
        b3.place(x=1100, y=370)

        b4 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE',command=exit)
        b4.place(x=1100, y=470)
        t.withdraw()
        mainloop()


    f1 = Frame(root, width=1370, height=30, bg="lightgreen", relief=SUNKEN)
    f1.pack(side=TOP)

    f2 = Frame(t, width=1370, height=650, bg="lightgreen", relief=SUNKEN)
    f2.pack(side=LEFT)

    f3 = Frame(t)
    f3.place(x=800, y=100)

    Heading = Label(f1, text="Book Directory or Library", font=('arial', 40, 'bold'))
    Heading.grid(row=0, column=0)

    # define labels
    l1 = Label(f2, text="Title", font=('arial', 20, 'bold'), bg='lightgreen')
    l1.place(x=10, y=30)

    l2 = Label(f2, text="Author", font=('arial', 20, 'bold'), bg='lightgreen')
    l2.place(x=10, y=80)

    l3 = Label(f2, text="Year", font=('arial', 20, 'bold'), bg='lightgreen')
    l3.place(x=10, y=130)

    l4 = Label(f2, text="ISBN", font=('arial', 20, 'bold'), bg='lightgreen')
    l4.place(x=10, y=180)

    l5 = Label(f2, text="Language", font=('arial', 20, 'bold'), bg='lightgreen')
    l5.place(x=10, y=230)

    l6 = Label(f2, text="Placed", font=('arial', 20, 'bold'), bg='lightgreen')
    l6.place(x=10, y=280)
    # Entry section
    title = StringVar()
    e1 = Entry(f2, font=('arial', 20, 'bold'), textvariable=title, insertwidth=5, justify=LEFT, bd=5, width=40)
    e1.place(x=150, y=30)

    author = StringVar()
    e2 = Entry(f2, font=('arial', 20, 'bold'), textvariable=author, insertwidth=5, justify=LEFT, bd=5, width=40)
    e2.place(x=150, y=80)

    year = StringVar()
    e3 = Entry(f2, font=('arial', 20, 'bold'), textvariable=year, insertwidth=5, justify=LEFT, bd=5, width=40)
    e3.place(x=150, y=130)

    isbn = StringVar()
    e4 = Entry(f2, font=('arial', 20, 'bold'), textvariable=isbn, insertwidth=5, justify=LEFT, bd=5, width=40)
    e4.place(x=150, y=180)

    placed = StringVar()
    e5 = Entry(f2, font=('arial', 20, 'bold'), textvariable=placed, insertwidth=5, justify=LEFT, bd=5, width=40)
    e5.place(x=150, y=280)

    lang = StringVar()
    e6 = Entry(f2, font=('arial', 20, 'bold'), textvariable=lang, insertwidth=5, justify=LEFT, bd=5, width=40)
    e6.place(x=150, y=230)

    # define button

    b1 = Button(f2, text="View All", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE')
    b1.place(x=150, y=380)

    b2 = Button(f2, text="Search Book", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE',command=pagetwo)
    b2.place(x=150, y=480)

    b3 = Button(f2, text="Add Entry", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE')
    b3.place(x=450, y=380)

    b3 = Button(f2, text="Update", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE')
    b3.place(x=450, y=480)

    s = Scrollbar(f3)
    s.pack(side=RIGHT, fill=Y)
    l = Listbox(f3, width=80, height=26, yscrollcommand=s.set)
    l.pack(side=LEFT, fill=BOTH)


    b4 = Button(f2, text="Exit", font=('arial', 10, 'bold'), width=25, bd=5, height=2, bg='#3D6DEE', command=exit)
    b4.place(x=1080, y=480)

    mainloop()
root=Tk()

root.title("Book Directory")

f1 = Frame(root, width=1370, height=30, relief=SUNKEN)
f1.pack(side=TOP)

f2 = Frame(root, width=1370, height=650, bg="powderblue", relief=SUNKEN)
f2.pack(side=LEFT)

f3 = Frame(root)
f3.place(x=500, y=300)

Heading = Label(f1, text="Book Directory", font=('arial', 40, 'bold'))
Heading.grid(row=0, column=0)

Heading1 = Label(f1,
                 text="Book Directory Apllication (BDA) that manages all your book records \n and also handle all library in collages and schools \n   If you like the app please email us on \n bhuvneshkumar2798@gamil.com ",
                 font=('arial', 15, 'italic'))
Heading1.grid(row=1, column=0)
# define labels
l1 = Label(f2, text="User-Name", font=('arial', 20, 'bold'), bg="powderblue")
l1.place(x=300, y=30)

l2 = Label(f2, text="Password", font=('arial', 20, 'bold'), bg="powderblue")
l2.place(x=300, y=80)

l3 = Label(f2, text="Mobile-No", font=('arial', 20, 'bold'), bg="powderblue")
l3.place(x=300, y=130)

# Entry section
name = StringVar()
e1 = Entry(f2, font=('arial', 20, 'bold'), textvariable=name, insertwidth=5, justify=LEFT, bd=5, width=35)
e1.place(x=500, y=30)

department = StringVar()
e2 = Entry(f2, font=('arial', 20, 'bold'), textvariable=department, insertwidth=5, justify=LEFT, bd=5, width=35)
e2.place(x=500, y=80)

roll_no = StringVar()
e3 = Entry(f2, font=('arial', 20, 'bold'), textvariable=roll_no, insertwidth=5, justify=LEFT, bd=5, width=35)
e3.place(x=500, y=130)

# define button

b1 = Button(f2, text="Login", font=('arial', 10, 'bold'), bd=5, width=25, height=2, bg='#3D6DEE',command=pageone)
b1.place(x=600, y=200)

root.mainloop()